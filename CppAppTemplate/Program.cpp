#include "stdafx.h"
#include "Application.h"
#include "AppWindow.h"

#ifdef _DEBUG
#include <vld.h>
#endif

#pragma comment(linker, "\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

int APIENTRY WinMain(_In_ HINSTANCE, _In_opt_ HINSTANCE, _In_ LPSTR, _In_ int)
{
	AppWindow appWindow;
	appWindow.Create();
	return Application::Run();
}
