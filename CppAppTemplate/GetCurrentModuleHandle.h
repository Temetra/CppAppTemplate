#pragma once
#include <windows.h>

inline HINSTANCE GetCurrentModuleHandle()
{
	HMODULE module = NULL;
	if (GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS, reinterpret_cast<LPCTSTR>(&GetCurrentModuleHandle), &module))
		return module;
	return NULL;
}
