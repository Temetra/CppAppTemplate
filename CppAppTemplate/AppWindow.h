#pragma once
#include "Window.h"
#include "CustomEditControl.h"

class AppWindow : public Window
{
public:
	AppWindow();
	virtual void Create() override;

protected:
	virtual LRESULT WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) override;

private:
	void OnCreate();
	void OnDestroy();
	CustomEditControl edit;
};

