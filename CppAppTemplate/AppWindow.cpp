#include "stdafx.h"
#include "AppWindow.h"

AppWindow::AppWindow() :
	edit(this)
{
}

LRESULT AppWindow::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	LRESULT result = 0;

	switch (message)
	{
	case WM_CREATE:
		OnCreate();
		return 0;
	case WM_DESTROY:
		OnDestroy();
		return 0;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

void AppWindow::Create()
{
	if (GetWindowHandle() != NULL) return;

	HMONITOR monitor = MonitorFromWindow(NULL, MONITOR_DEFAULTTONEAREST);
	MONITORINFO monitorInfo;
	monitorInfo.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &monitorInfo);

	static const auto configureWindowClass = [&](WNDCLASSEX & wcex)
	{
		wcex.lpszClassName = L"AppWindowTest";
		wcex.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
		wcex.hCursor = LoadCursor(NULL, MAKEINTRESOURCE(IDC_ARROW));
		wcex.hbrBackground = CreateSolidBrush(RGB(255,255,255));
	};

	static const auto configureWindowStruct = [&](CREATESTRUCT & cs)
	{
		cs.lpszClass = L"AppWindowTest";
		cs.lpszName = L"Test Window";
		cs.style = WS_OVERLAPPEDWINDOW | WS_VISIBLE;
		cs.dwExStyle = NULL;
		cs.hwndParent = NULL;
		cs.cx = 640;
		cs.cy = 480;
		cs.x = (monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left - cs.cx) / 2;
		cs.y = (monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top - cs.cy) / 2;
		cs.hMenu = NULL;
	};

	Window::Create(configureWindowClass, configureWindowStruct);
}

void AppWindow::OnCreate()
{
	edit.SetCoords(8, 8, 175, 20);
	edit.Create();
}

void AppWindow::OnDestroy()
{
	PostQuitMessage(0);
}
